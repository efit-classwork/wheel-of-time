public class WoT {
    public static void  main(String[] args){
//        String pattern = "The wheel weaves as the wheel wills.";
//        System.out.println(pattern);
        Human Rand = new Human();
        Rand.setName("Rand al`Thor");
        Rand.setCanChannel(true);
        Rand.setDarkFriend(false);
        System.out.println(Rand.createStringOfDarkFriend());
        Trolloc Narg = new Trolloc();
        Narg.setName("Narg");
        Narg.setDarkFriend(true);
        System.out.println(Narg.createStringOfDarkFriend());
        Ogier Loial = new Ogier();
        Loial.setName("Loial son of Arent son of Halan");
        System.out.println(Loial.createStringOfDarkFriend());
    }
}
