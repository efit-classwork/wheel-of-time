public class Person {
    private String name;
    private boolean darkFriend;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isDarkFriend() {
        return darkFriend;
    }

    public void setDarkFriend(boolean darkFriend) {
        this.darkFriend = darkFriend;
    }
    String createStringOfDarkFriend(){
        if(isDarkFriend()){
            return name + " is a dark friend.";
        } else if(!isDarkFriend()){
            return name + " walks in the light.";
        } else {
            return name + " is a questionable character.";
        }
    }
}
